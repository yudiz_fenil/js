const user = {
    name: "Your Name",
    address: {
        personal: {
            line1: "101",
            line2: "street Line",
            city: "NY",
            state: "WX",
            coordinates: {
                x: 35.12,
                y: -21.49,
            },
        },
        office: {
            city: "City",
            state: "WX",
            area: {
                landmark: "landmark",
            },
            coordinates: {
                x: 35.12,
                y: -21.49,
            },
        },
    },
    contact: {
        phone: {
            home: "xxx",
            office: "yyy",
        },
        other: {
            fax: '234'
        },
        email: {
            home: "xxx",
            office: "yyy",
        },
    }
};


let newObj = {};

    const singleObj = function (oUser, sKey) {
        for (let k in oUser) {
            const newKey = sKey + k;
            const value = oUser[k]
            if (typeof value == "object") {
                singleObj(value, newKey + "_")
            } else {
                newObj[newKey] = value
            }
        }
        return newObj;
    }

console.log(singleObj(user, "user_"))

// let nBankBalance = 30000;
let nBankBalance = prompt("Enter Bank Balance (Minimum 10000)");
const nMobilePrice = 7000;
const nAccessories = 500;
nTotalPrice = nMobilePrice + nAccessories;
let nTaxRate = 0;
let nTotalPurchase = 0;
nSpendingThreshold = 10000;
let nCount = 0;
var sItem;

function getTax(nTotalPrice) {
    return (nTotalPrice) * 18 / 100;
}

while (nTotalPrice < nBankBalance) {
    // If Spending Threshold is less than Total Price than it will buy only mobile not accessories
    if (nTotalPrice < nSpendingThreshold) {
        nTaxRate = getTax(nTotalPrice);
        nFinalPrice = nTotalPrice + nTaxRate;
        // console.log("Mobile and Accessories price:", nFinalPrice);
        nTotalPurchase = nTotalPurchase + nFinalPrice;
        nBankBalance = nBankBalance - nFinalPrice;
        sItem = "Mobile with Accessories";
    } else {
        nTotalPrice = nMobilePrice;
        nTaxRate = getTax(nTotalPrice);
        nFinalPrice = nTotalPrice + nTaxRate;
        // console.log("Mobile price:", nFinalPrice);
        nTotalPurchase = nTotalPurchase + nFinalPrice;
        nBankBalance = nBankBalance - nFinalPrice;
        sItem = "Mobile without Accessories";
    }
    nCount++;
}
document.getElementById('item').innerHTML = sItem;
document.getElementById('totalitem').innerHTML = nCount;
document.getElementById('price').innerHTML = "$"+nFinalPrice;
document.getElementById('spendingthreshold').innerHTML = "$"+nSpendingThreshold;
document.getElementById('totalpurchase').innerHTML = "$"+nTotalPurchase.toFixed(2);
document.getElementById('bankbalance').innerHTML = "$"+nBankBalance.toFixed(2);
// console.log("Total Item:", nCount);
// console.log("Total purchase amount is:", nTotalPurchase);
// console.log("Total Bank Balance left:", nBankBalance);
